> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Kristopher Mangahas

### Project 2 # Requirements:

*Three Parts:*

1. Course Title, Your name, assignment requirements as of A1
2. Screenshots as per below examples;
3. Link to local lis4381 web app: http://localhost/repos/lis4381



#### Assignment Screenshots:


*Screenshot of Delete prompt*:

![Screenshot of delete](img/delete.png)

*Screenshot of Edit menu*:

![Screenshot of edit](img/edit.png)

*Screenshot of P2*:

![Screenshot of delete](img/p2.png)

*Screenshot of Rss Feed*:

![Screenshot of delete](img/rss.png)

*Screenshot of Edit Error*:

![Screenshot of delete](img/error.png)