bit> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kristopher Mangahas



[A1 README.md](a1/README.md)

* Install GIT
* Install AMPPS
* Install Java - Create Hello World Application 
* Install Android Studio - Create My First Application
* Provide picture documentation of created Applications
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
* Provide git command descriptions

[A2 README.md](a2/README.md) 

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)



[A3 README.md](a3/README.md) 

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Links to the following files:

1. a3.mwb
2. a3.sql

[P1 README.md](p1/README.md) 

* Course Title, you name, assignment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;

[A4 README.md](a4/README.md) 

* Course Title, Your name, assignment requirements as of A1
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381

[A5 README.md](a5/README.md) 

* Course Title, Your name, assignment requirements as of A1
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381


[P2 README.md](p2/README.md) 

* Course Title, Your name, assignment requirements as of A1
* Screenshots as per below examples;
* Link to local lis4381 web app: http://localhost/repos/lis4381
