> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Kristopher Mangahas

### Assignment 5 # Requirements:

*Three Parts:*

1. Course Title, Your name, assignment requirements as of A1
2. Screenshots as per below examples;
3. Link to local lis4381 web app: http://localhost/repos/lis4381



#### Assignment Screenshots:


*Screenshot of Placeholders*:

![Screenshot of ampps running](img/placeholder.png)

*Screenshot of Table*:

![Screenshot of ampps running](img/table.png)