> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Kristopher Mangahas

### Project 1 # Requirements:

*Three Parts:*

* Course Title, you name, assignment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;




#### Assignment Screenshots:


*Screenshot of first user interface*:

![Screenshot of ampps running](img/first.png)


*Screenshot of second user interface*:

![Screenshot of java application running](img/second.png)



