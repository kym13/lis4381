> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Kristopher Mangahas

### Assignment 2 # Requirements:

*Four Parts:*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application's first user interface;
3. Screenshot of running application's second user interface;
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)




#### Assignment Screenshots:

*Screenshot of first user interface*:

![Screenshot of ampps running](img/first.png)


*Screenshot of second user interface*:

![Screenshot of java application running](img/second.png)



