> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Kristopher Mangahas

### Assignment 3 # Requirements:

*Four Parts:*

1. Screenshot of ERD
2. Screenshot of running application's first user interface
3. Screenshot of running application's second user interface
4. Links to the following files


* [a3 Database](a3.mwb)
* [a3 sql](a3.sql)


#### Assignment Screenshots:

*Screenshot of ERD*

![Screenshot of ERD](img/erd.png)


*Screenshot of first user interface*:

![Screenshot of ampps running](img/first.png)


*Screenshot of second user interface*:

![Screenshot of java application running](img/second.png)



